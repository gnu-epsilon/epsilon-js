# UI

HISTORY = []
HISTORY_INDEX = -1
CLI = jQuery("#cli")
INPUT = jQuery("#cli input")
CONTAINER = jQuery('#container')


def append(command, output, debug):
    cycle = jQuery('<div class="cycle"></div>')
    cycle.append('<div class="command"> εjs &gt; ' + command + '</div>')
    cycle.append('<div class="output">' + output + '</div>')
    cycle.append('<div class="debug">' + debug + '</div>')
    CLI.before(cycle)


def on_key_up(event):
    global HISTORY_INDEX
    if (event.keyCode == 13):
        command = INPUT.val()
        HISTORY.push(command)
        HISTORY_INDEX = HISTORY.length - 1
        INPUT.val('')
        debug = ''
        try:
            output = epsilon(read(tokenize(command)))
        except:
            output = '<span class="error">' + __exception__ + '</span>'
        append(command, output, debug)
    elif (event.keyCode == 38):
        INPUT.val(HISTORY[HISTORY_INDEX])
        HISTORY_INDEX -= 1
    elif (event.keyCode == 40):
        HISTORY_INDEX += 1
        INPUT.val(HISTORY[HISTORY_INDEX])


INPUT.keyup(on_key_up)


### EPSILON 

# PARSER

OPEN = {'OPEN': 'OPEN'}
CLOSE = {'CLOSE': 'CLOSE'}


def tokenize(code):
    out = []
    token = ''
    for char in code:
        if char == '(':
            out.push(OPEN)
        elif char == ')':
            if token:
                out.push(token)
                token = ''
            out.push(CLOSE)
        elif char == ' ':
            if token:
                out.push(token)
                token = ''
            else:
                continue
        else:
            token += char
    return out


def atom(token):
    number = Number(token)
    if isNaN(number):
        return token
    else:
        return number


def read(tokens):
    if tokens.length == 0:
        raise 'SyntaxError'
    token = tokens.shift()
    if token == OPEN:
        L = []
        while tokens[0] != CLOSE:
            L.push(read(tokens))
        tokens.shift()
        return L
    elif token == CLOSE:
        raise 'SyntaxError'
    else:
        return atom(token)


# RUNTIME

STACK = [{}]


def stack_get(key):
    index = STACK.length - 1
    while index >= 0:
        item = STACK[index]
        value = item[key]
        if value:
            return value
        else:
            index -= 1
            continue


def stack_set(key, value):
    index = STACK.length - 1
    STACK[index][key] = value


def stack_push():
    STACK.push({})


def stack_pop():
    STACK.pop()


# E0 procedures


def e0_if_in(discriminant, constants, then_branch, else_branch):
    discriminant = epsilon(discriminant)  # eval
    for constant in constants:
        constant = epsilon(constant)  # eval
        if discriminant == constant:
            return epsilon(then_branch)
    # else
    return epsilon(else_branch)
STACK[0]['e0:if-in'] = e0_if_in


def e0_bundle():
    args = Array.prototype.slice.call(arguments, 0)
    index = 0
    for arg in args:
        args[index] = epsilon(arg)
    return args
STACK[0]['e0:bundle'] = e0_bundle


def e0_let(variables, expression, body):
    variable = variables[0]
    expression = epsilon(expression[0])
    stack_push()
    stack_set(variable, expression)
    v = epsilon(body)
    stack_pop()
    return v

STACK[0]['e0:let'] = e0_let


def e0_value(value):
    return v

STACK[0]['e0:value'] = e0_value


# E1 procedures

def e1_define(name_or_list, body):
    name = name_or_list[0]
    if Object.prototype.toString.call(name_or_list) == '[object Array]':
        # it's a procedure
        args = name_or_list.slice(1)
        body.arguments = args
        STACK[0][name] = body
    else:
        STACK[0][name] = body


# main procedure to eval epsilon code


def epsilon(sexpr):
    if not isNaN(sexpr):
        return sexpr
    if Object.prototype.toString.call(sexpr) == '[object String]':
        return stack_get(sexpr)
    else:  # it's call
        func = stack_get(sexpr[0])
        if not func:
            raise sexpr[0] + ' is not defined'
        args = sexpr.slice(1)
        if func.is_javascript == True:
            return func.apply(None, args)
        else:
            # it's a epsilon sepxr
            pass

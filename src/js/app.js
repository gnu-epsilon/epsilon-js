HISTORY = [];
HISTORY_INDEX = -1;
CLI = jQuery("#cli");
INPUT = jQuery("#cli input");
CONTAINER = jQuery("#container");
var append = function(command, output, debug) {
    var cycle;
    cycle = jQuery('<div class="cycle"></div>');
    cycle.append((('<div class="command"> εjs &gt; ' + command) + "</div>"));
    cycle.append((('<div class="output">' + output) + "</div>"));
    cycle.append((('<div class="debug">' + debug) + "</div>"));
    CLI.before(cycle);
};
var on_key_up = function(event) {
    var debug,command,output;
    if((event.keyCode == 13)) {
        command = INPUT.val();
        HISTORY.push(command);
        HISTORY_INDEX = (HISTORY.length - 1);
        INPUT.val("");
        debug = "";
        try {
            output = epsilon(read(tokenize(command)));
        }
        catch(__exception__) {
            output = (('<span class="error">' + __exception__) + "</span>");
        }
        append(command, output, debug);
    }
    else {
        if((event.keyCode == 38)) {
            INPUT.val(HISTORY[HISTORY_INDEX]);
            HISTORY_INDEX = HISTORY_INDEX - 1;
        }
        else {
            if((event.keyCode == 40)) {
                HISTORY_INDEX = HISTORY_INDEX + 1;
                INPUT.val(HISTORY[HISTORY_INDEX]);
            }
        }
    }
};
INPUT.keyup(on_key_up);
OPEN = {"OPEN":"OPEN"};
CLOSE = {"CLOSE":"CLOSE"};
var tokenize = function(code) {
    var out,token;
    out = [];
    token = "";
    var iterator_char = code;
    for (var char_iterator_index=0; char_iterator_index < iterator_char.length; char_iterator_index++) {
        var char = iterator_char[char_iterator_index];
        if((char == "(")) {
            out.push(OPEN);
        }
        else {
            if((char == ")")) {
                if(token) {
                    out.push(token);
                    token = "";
                }
                out.push(CLOSE);
            }
            else {
                if((char == " ")) {
                    if(token) {
                        out.push(token);
                        token = "";
                    }
                    else {
                        continue;
                    }
                }
                else {
                    token = token + char;
                }
            }
        }
    }
    return out;
    return undefined;
};
var atom = function(token) {
    var number;
    number = Number(token);
    if(isNaN(number)) {
        return token;
        return undefined;
    }
    else {
        return number;
        return undefined;
    }
};
var read = function(tokens) {
    var L,token;
    if((tokens.length == 0)) {
        throw "SyntaxError";
    }
    token = tokens.shift();
    if((token == OPEN)) {
        L = [];
        while((tokens[0] != CLOSE)) {
            L.push(read(tokens));
        }
        tokens.shift();
        return L;
        return undefined;
    }
    else {
        if((token == CLOSE)) {
            throw "SyntaxError";
        }
        else {
            return atom(token);
            return undefined;
        }
    }
};
STACK = [{}];
var stack_get = function(key) {
    var index,item,value;
    index = (STACK.length - 1);
    while((index >= 0)) {
        item = STACK[index];
        value = item[key];
        if(value) {
            return value;
            return undefined;
        }
        else {
            index = index - 1;
            continue;
        }
    }
};
var stack_set = function(key, value) {
    var index;
    index = (STACK.length - 1);
    STACK[index][key] = value;
};
var stack_push = function() {
    STACK.push({});
};
var stack_pop = function() {
    STACK.pop();
};
var e0_if_in = function(discriminant, constants, then_branch, else_branch) {
    var constant;
    discriminant = epsilon(discriminant);
    var iterator_constant = constants;
    for (var constant_iterator_index=0; constant_iterator_index < iterator_constant.length; constant_iterator_index++) {
        var constant = iterator_constant[constant_iterator_index];
        constant = epsilon(constant);
        if((discriminant == constant)) {
            return epsilon(then_branch);
            return undefined;
        }
    }
    return epsilon(else_branch);
    return undefined;
};
STACK[0]["e0:if-in"] = e0_if_in;
var e0_bundle = function() {
    var index,args;
    args = Array.prototype.slice.call(arguments, 0);
    index = 0;
    var iterator_arg = args;
    for (var arg_iterator_index=0; arg_iterator_index < iterator_arg.length; arg_iterator_index++) {
        var arg = iterator_arg[arg_iterator_index];
        args[index] = epsilon(arg);
    }
    return args;
    return undefined;
};
STACK[0]["e0:bundle"] = e0_bundle;
var e0_let = function(variables, expression, body) {
    var variable,v;
    variable = variables[0];
    expression = epsilon(expression[0]);
    stack_push();
    stack_set(variable, expression);
    v = epsilon(body);
    stack_pop();
    return v;
    return undefined;
};
STACK[0]["e0:let"] = e0_let;
var e0_value = function(value) {
    return v;
    return undefined;
};
STACK[0]["e0:value"] = e0_value;
var e1_define = function(name_or_list, body) {
    var name,args;
    name = name_or_list[0];
    if((Object.prototype.toString.call(name_or_list) == "[object Array]")) {
        args = name_or_list.slice(1);
        body.arguments = args;
        STACK[0][name] = body;
    }
    else {
        STACK[0][name] = body;
    }
};
var epsilon = function(sexpr) {
    var args,func;
    if(!isNaN(sexpr)) {
        return sexpr;
        return undefined;
    }
    if((Object.prototype.toString.call(sexpr) == "[object String]")) {
        return stack_get(sexpr);
        return undefined;
    }
    else {
        func = stack_get(sexpr[0]);
        if(!func) {
            throw (sexpr[0] + " is not defined");
        }
        args = sexpr.slice(1);
        if((func.is_javascript == true)) {
            return func.apply(undefined, args);
            return undefined;
        }
        else {
            /* pass */
        }
    }
};
